<?php
require_once __DIR__ . '/vendor/autoload.php';
session_start();

use App\Chicken;
use App\Cow;
use App\Farm;

if (!$_SESSION['cow'] && !$_SESSION['chicken']) {
    $_SESSION['cow'] = 10;
    $_SESSION['chicken'] = 20;
    $_SESSION['day'] = 1;
    $_SESSION['temp'] = 0;
}

$farm = new Farm();
$cow = new Cow();
$chicken = new Chicken();
$result = new \App\PrintResults();

if ($_SESSION['temp'] == 0)
{
    $farm->setAnimals($cow, $_SESSION['cow']);
    $farm->setAnimals($chicken, $_SESSION['chicken']);
    $_SESSION['temp'] +=1;
}

if (isset($_POST['add'])) {
    switch ($_POST['add']) {
        case 'cow':
            $farm->setAnimals($cow, 1,'');
            break;
        case 'chicken':
            $farm->setAnimals($chicken, 1,'');
            break;
        case 'getAllProductsOfTheDay':
            $farm->getAmountOfProducts();
            $_SESSION['day'] += 1;
            break;
    }
} elseif (isset($_POST['typeOfAnimal'])) {
    switch ($_POST['typeOfAnimal']) {
        case 'cow':
            $farm->setAnimals($cow, 1, $_POST['nameOfAnimal']);
            break;
        case 'chicken':
            $farm->setAnimals($chicken, 1, $_POST['nameOfAnimal']);
            break;
    }
}

$result->getAmountOfAnimals($farm->getOfAnimals());
$result->printProducts($farm->getAllProducts());
?>

<form method="post">
    <button name="add" type="submit" value="cow">Добавить корову</button>
    <button name="add" type="submit" value="chicken">Добавить курицу</button>
    <button name="add" type="submit" value="getAllProductsOfTheDay">Собрать урожай</button>
    <br><br>
    <input type="text" name="typeOfAnimal" placeholder="Введите тип животного">
    <input type="text" name="nameOfAnimal" placeholder="Введите имя животного">
    <button type="submit">Добавить животное</button>
</form>





