<?php

namespace App;

//use http\Exception\InvalidArgumentException;

class Farm
{
    private $allAnimals = [];
    private $allProducts = [];

    public function setAnimals($objectAnimal, $amountOfAnimals, $nameOfAnimal = "name")
    {
       if (!is_object($objectAnimal))
        {
           echo "<br>" . "Object format is invalid!" . "<br>";
        }

        if (!is_int($amountOfAnimals))
        {
            echo "<br>" . "Number of animals format is invalid!" . "<br>";
        }

        if (!ctype_alpha($nameOfAnimal)) {
            echo "<br>" . "У животного не текстовое имя!" . "<br>";
        }

        if ($_SESSION['temp'] > 0)
        {
            unset($this->allAnimals );
            $this->allAnimals = $_SESSION['animals'];
        }

        if (isset($this->allAnimals[$objectAnimal->getName()]) && ctype_alpha($nameOfAnimal)) {
            $lastAnimal = count($this->allAnimals[$objectAnimal->getName()]);
            for ($i = $lastAnimal; $i < $amountOfAnimals + $lastAnimal; $i++) {
                $animal = new $objectAnimal;
                $animal->setIndex($i);
                $animal->setNameOfAnimal($nameOfAnimal);
                $this->allAnimals[$animal->getName()][] = $animal;
                $_SESSION[$objectAnimal->getName()] += 1;
            }
        } else {
            for ($i = 0; $i < $amountOfAnimals; $i++) {
                $animal = new $objectAnimal;
                $animal->setIndex($i);
                $animal->setNameOfAnimal($nameOfAnimal);
                $this->allAnimals[$animal->getName()][] = $animal;
            }
        }

        unset($_SESSION['animals']);
        $_SESSION['animals'] = $this->allAnimals;
    }

    public function getAmountOfProducts()
    {
        $this->allAnimals = $_SESSION['animals'];

        if (isset($_SESSION['products'])) {
            $this->allProducts = $_SESSION['products'];
            foreach ($this->allProducts as $key => $value) {
                $this->allProducts[$key][] = 'productsOfTheWeek';
                $this->allProducts[$key]['productsOfTheWeek'] += $this->allProducts[$key]['productsOfTheDay'];
                $this->allProducts[$key]['productsOfTheDay'] = 0;
            }
        }

        foreach ($this->allAnimals as $key => $value) {
            foreach ($value as $product) {
                if(empty($this->allProducts[$product->getProduct()])) {
                    $this->allProducts[$product->getProduct()] = $product->getProduct();
                    $this->allProducts[$product->getProduct()] = ['productsOfTheDay'];
                    $this->allProducts[$product->getProduct()] = ['productsOfTheWeek'];
                }
                $this->allProducts[$product->getProduct()][] = ['productsOfTheDay'];
                $this->allProducts[$product->getProduct()][] = ['productsOfTheWeek'];
                $this->allProducts[$product->getProduct()]['productsOfTheDay'] += $product->getAmountOfProductFromOneAnimal();
            }
        }
        $_SESSION['products'] = $this->allProducts;
    }

    public function getAllProducts()
    {
        return $this->allProducts;
    }

    public function getOfAnimals()
    {
        return $this->allAnimals;

    }
}

