<?php

namespace App;

abstract class Animal
{
    protected $name;
    protected $product;
    protected $minAmountOfProduct;
    protected $maxAmountOfProduct;
    protected $indexOfAnimal;
    protected $nameOfAnimal;


    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setProduct($product)
    {
        $this->product = $product;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setMinAmountOfProduct($minAmountOfProduct)
    {
        $this->minAmountOfProduct = $minAmountOfProduct;
    }

    public function getMinAmountOfProduct()
    {
        return $this->minAmountOfProduct;
    }

    public function setMaxAmountOfProduct($maxAmountOfProduct)
    {
        $this->maxAmountOfProduct = $maxAmountOfProduct;
    }

    public function getMaxAmountOfProduct()
    {
        return $this->maxAmountOfProduct;
    }

    public function setIndex($indexOfAnimal)
    {
        $this->indexOfAnimal = $indexOfAnimal;
    }

    public function getIndex()
    {
        return $this->indexOfAnimal;
    }

    public function getAmountOfProductFromOneAnimal()
    {
        return mt_rand($this->minAmountOfProduct, $this->maxAmountOfProduct);
    }

    public function setNameOfAnimal($nameOfAnimal)
    {
        $this->nameOfAnimal = $nameOfAnimal;
    }

    public function getNameOfAnimal()
    {
        return $this->nameOfAnimal;
    }
}

