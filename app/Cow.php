<?php

namespace App;

class Cow extends Animal
{
    public function __construct()
    {
        $this->name = 'cow';
        $this->product = 'milk';
        $this->minAmountOfProduct = 8;
        $this->maxAmountOfProduct = 12;
        $this->indexOfAnimal = 0;
        $this->nameOfAnimal = '';
    }
}
