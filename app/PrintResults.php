<?php


namespace App;
use App\Farm;

class PrintResults
{
    public function printProducts($allProducts)
    {
        if (($_SESSION['day'] - 1) % 7 == 0) {
            echo 'За ' . ($_SESSION['day'] - 1) / 7 . ' нед.' . ' собрано ' . '<br>' ;
            foreach ($allProducts as $product => $amount) {
                echo $product . ': ' . $amount['productsOfTheWeek'] . '<br>' ;
            }
        }
    }
    public function getAmountOfAnimals($allAnimals)
    {
        echo 'На ферме живет' . '<br>';
        foreach ($allAnimals as $key => $value) {
            echo ucfirst($key) . 's' . ' ' . count($value) . '<br>';
        }
    }

}