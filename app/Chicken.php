<?php

namespace App;

class Chicken extends Animal
{
    public function __construct()
    {
        $this->name = 'chicken';
        $this->product = 'eggs';
        $this->minAmountOfProduct = 0;
        $this->maxAmountOfProduct = 1;
        $this->indexOfAnimal = 0;
        $this->nameOfAnimal = '';
    }

}
